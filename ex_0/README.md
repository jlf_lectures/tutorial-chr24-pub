# Exercice 0: Graph Construction

## Setup

1. Download the file `WHO_txt.zip` and extract the WHO resolution in text format. Each file is a resolution.

2. Download the file `WHO_metadata.json` wich contains the title and the year of each resolution

3. Be sure you have the folowing python packages installed (available with pip, conda, etc.):
  - networkx
  - gravis

*Warning:* The OCR comes directly from the WHO, and it is pretty bad.

## Graph building

Execute the script `graph_0.py`. A representation of the graph should appear in your web browser. Play with the layout option.

## Connected components

The graph is composed of disconnected components. We can split it to simplify the interpretation.

Uncomment line 77, and try displaying different connected components. By varying the argument `n`.
