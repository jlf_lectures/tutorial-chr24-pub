# Exercice 1: Node metrics

## Setup

Copy the data of exercise 0 into the present directory.

## Node degrees

Execute the script `graph_1.py`. It has been modified to compute and display a few graph metrics.

Here we compute: 

  - the *in-degree*: the number of incoming references
  - the *out-degree*: the number of outgoing references

We just display the 12 highest nodes for each metric.

Try interpreting the results.

Try looking at the other components.

## Additional metrics

Uncomment lines 95-97 to compute the *betweenness centrality*. This metric is higher when many path in the network pass through a given node. It means that the node is able to link disconnected subjetcs. Try interpreting the results.

Follow the same model to compute the number of *triangle* of each node, looking at the documentation: <https://networkx.org/documentation/stable/reference/algorithms/clustering.html>. The graph has to be transformed in an undirected graph with the methode `.to_undirected()`. The number of triangles represent to number of connections that are themselves connected.


