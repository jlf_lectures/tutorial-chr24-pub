# Exercice 2: Neighborhood

## Setup

Copy the data of exercise 0 into the present directory.

## Neighborhood

Execute the script `graph_2.py`. It will display the neighborhood
of a chosen resolution.

Try inputing different resolution and visualise the neighborhood.
