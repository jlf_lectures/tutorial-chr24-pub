import networkx as nx
from pathlib import Path
import os
import re
import gravis as gv
import json

# Simple method to recognize the resolution
#  ids
wha_id = re.compile(r"""WHA\d\d?\.\d+""")


# Reads files and extract the references
# Returns a dict where the keys are the resolutions
#   ids and the values are a set of references
def read_files(dir: Path) -> dict[str, set[str]]:
    result = {}
    for fname in os.listdir(dir):
        name = wha_id.search(fname)
        if name is None:
            continue
        reso_id = name.group(0)
        refs = set()
        with open(dir / fname) as f:
            body = f.read()
            for ref in wha_id.finditer(body):
                refs.add(ref.group(0))
            if reso_id in refs:
                refs.remove(reso_id)
        result[reso_id] = refs
    return result


# Builds the graph of citation
def buildGraph(refs: dict[str, set[str]]) -> nx.DiGraph:
    # DiGraph is a directed Graph
    # because citations are directed
    G = nx.DiGraph()
    for u, vs in refs.items():
        for v in vs:
            G.add_edge(u, v)
    return G


# Adds to each resolution the title and the year
def add_metadata(fname: Path, G: nx.DiGraph) -> None:
    with open(fname) as f:
        meta = json.load(f)
        for r in meta:
            G.add_node(r, **meta[r])


# Display the graph in the browser
def display(G: nx.DiGraph) -> None:
    fig = gv.vis(
        G,
        node_label_data_source="title",
        edge_size_factor=0.5,
    )
    fig.display()


# Extract a single (weakly) connected component
#   n is the index of the component in decreasing order
#   (ie n=0 for the largest, n=1 for the second and so on)
def keep_connected_component(G: nx.DiGraph, n_comp: int) -> nx.DiGraph:
    comps = nx.weakly_connected_components(G)
    comps = sorted(comps, key=len, reverse=True)
    return G.subgraph(comps[n_comp])


# Prints the 12 nodes with highest metric score
def print_metrics(G: nx.DiGraph, metric: dict[str, float | int]) -> None:
    n_best = sorted(metric, reverse=True, key=metric.get)[:12]
    for b in n_best:
        attr = G.nodes[b]
        print(f"""{metric[b]}: {b} - {attr["title"]} ({attr["year"]})""")


def neighborhood(G: nx.DiGraph, node_id: str) -> nx.DiGraph:
    nhood = nx.all_neighbors(G, node_id)
    return nx.induced_subgraph(G, nhood)


if __name__ == "__main__":
    dir = Path("WHO_txt")
    metadata = Path("WHO_metadata.json")
    refs = read_files(dir)
    G = buildGraph(refs)
    add_metadata(metadata, G)
    G = neighborhood(G, "WHA43.13")
    display(G)
